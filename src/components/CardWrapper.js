import React from 'react';
import CardBackSide from './CardBackSide';
import CardFrontSide from './CardFrontSide';


const CardWrapper = () => {
    return (
        <div className="cardWrapper">
            <CardBackSide />
            <CardFrontSide />
        </div>
    );
}

export default CardWrapper;