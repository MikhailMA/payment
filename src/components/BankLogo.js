import React from 'react';
import alfaAdditionalLogo from '../img/alfaAdditionalLogo.svg';

const BankLogo = () => {
    return (
        <div className = 'BankLogo'>
            <img src={alfaAdditionalLogo}
                 id = 'BankLogo'
                 alt = 'BankLogo'
                 // sizes = '1px'
            />

        </div>
    );
}

export default BankLogo;