import React from 'react';

const Month = () => {
    return (
        <div className = 'Month'>
            <input type = 'text'
                   id = 'month'
                   size = '2'
                   placeholder = 'MM'
                   maxLength = '2'
                   width = '100%'
                   // height = '100%'

            />
        </div>
    );
}

export default Month;