import React from 'react';
import Button from "arui-feather/button";

const Pay = () => {
    return (
        <div className = 'Pay'>
            <Button size = 'xl' view = 'extra'>
                Оплатить
            </Button>
        </div>
    );
}

export default Pay;