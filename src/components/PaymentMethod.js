import React from 'react';
import Heading from "arui-feather/heading";

const PayMethod = () => {
    return (
        <div className = 'PayMethod'>
            <Heading size = 'xs' theme='alfa-on-white'>
                Оплата банковской картой
            </Heading>
        </div>
    );
}

export default PayMethod;