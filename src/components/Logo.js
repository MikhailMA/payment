import React from 'react';
import alfaAdditionalLogo from '../img/alfaAdditionalLogo.svg';

const Logo = () => {
    return (
        <div className = 'Logo'>
            <img src={alfaAdditionalLogo}
                 className="App-logo"
                 alt="Logo"
            />

        </div>
    );
}

export default Logo;