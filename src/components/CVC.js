import React from 'react';

const CVC = () => {
    return (
        <div className = 'CVC'>
            <input type = 'text'
                   id = 'CVC'
                   size = '9'
                   placeholder = 'CVC'
                   maxLength = '3'
                   width='100%'
            />
        </div>
    );
}

export default CVC;