import React from 'react';

const CardHolder = () => {
    return (
        <div className = 'CardHolder'>
            <input type = 'text'
                   id = 'cardholderName'
                   // size = 'xl'
                   placeholder = 'CARDHOLDER NAME'
                   width = '100%'
                   height = '100%'
                   size = '37'
            />
        </div>
    );
}

export default CardHolder;