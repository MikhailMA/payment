import React from 'react';

const Year = () => {
    return (
        <div className = 'Year'>
            <input type = 'text'
                   id = 'year'
                   size = '2'
                   placeholder = 'YY'
                   maxLength = '2'
                   width = '100%'
            />
        </div>
    );
}

export default Year;