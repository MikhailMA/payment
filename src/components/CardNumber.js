import React from 'react';

const CardNumber = () => {
    return (
        <div className = 'CardNumber'>
            <input type = 'text'
                   id = 'cardNumber'
                   placeholder = 'CARD NUMBER'
                   maxLength = '19'
                   width = '100%'
                   size = '31'

            />
        </div>
    );
}

export default CardNumber;