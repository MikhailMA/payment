import React from 'react';
import './App.css';
import Logo from "./components/Logo";
import PayMethod from "./components/PaymentMethod";
import CardFrontSide from "./components/CardFrontSide";
import CardNumber from "./components/CardNumber";
import Month from "./components/Month";
import Year from "./components/Year";
import CardHolder from "./components/CardHolder";
import CVC from "./components/CVC";
import Pay from "./components/Pay";
import Footer from "./components/Footer";
import Sum from "./components/Sum";
import BankLogo from "./components/BankLogo";
import Text from "./components/Text";


const App = () => {
    return(
        <div className = 'App'>
            <div className="box box1"><Logo/></div>
            <div className="box box2"><PayMethod/></div>
            <div className="box box15"><Sum/></div>
            <div className="box box9"><CardFrontSide/></div>
            <div className="box box10"><cardBackSideLine /></div>
            <div className="box box11"><CVC/></div>
            <div className="box box12"><Pay/></div>
            <div className="box box13"><Footer/></div>
            <div className="box box4"><CardFrontSide/></div>
            <div className="box box5"><CardNumber/></div>
            <div className="box box6"><Month/></div>
            <div className="box box7"><Year/></div>
            <div className="box box8"><CardHolder/></div>
            <div className="box box14"><BankLogo/></div>
            <div className="box box3"><Text/></div>
        </div>
    );
}

export default App;
